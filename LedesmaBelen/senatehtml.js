//Guardamos elementos del HTML en variables

let selectState = document.getElementById("estados");
let tabla = document.getElementById("tbody-house")
let checkboxs = document.querySelectorAll("input[type='checkbox']");

//-------------------------------------------------------------------------
//Creamos la tabla por default con todos los miembros
dataHouse.results[0].members.forEach(members =>{
    let fila = document.createElement("tr");
    fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
    tabla.appendChild(fila);
});

//-------------------------------------------------------------------------
/*
//Contando los independientes
let indepe = [];
dataHouse.results[0].members.forEach(members =>{
    if(members.party == "I"){
        indepe.push(members);
    }
});
console.log(indepe)*/

//-------------------------------------------------------------------------
//Creo array para contar los estados sin repetir
function countState(dataHouse){
    let array = [];
    dataHouse.results[0].members.forEach(members => {
    
    if (!(array.some(estado => estado == members.state))){
        array.push(members.state);
    }         
    });
    
    array.forEach(x =>{
        let option = document.createElement("option");
        option.innerHTML = `${x}`;
        selectState.appendChild(option);
    });  
}

countState(dataHouse);

//-------------------------------------------------------------------------
//Guardamos los options en una variable
let opciones = document.querySelectorAll('option') 

//Cuando el cliente clickea el select, trabajamos con los options y los checked que podrian estar checkeados

opciones.forEach(option => option.addEventListener('click',function(){
    let loschekeados = (Array.from(checkboxs).filter(checkbox => checkbox.checked));
    if(loschekeados.length === 0){
    let options = (Array.from(opciones).map(option => option.value));
    if(this.value == ""){
        dataHouse.results[0].members.forEach(members =>{
            let fila = document.createElement("tr");
            fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
            tabla.appendChild(fila);
        });
    }else if(options.includes(this.value)){
        tabla.innerHTML= "";
        renderState(this.value)
    }
    }else if(loschekeados.length > 0){
        tabla.innerHTML = "";
        loschekeados.forEach(checkeado => {
                dataHouse.results[0].members.forEach(member =>{
                    if(member.party === checkeado.value && option.value == member.state){
                    let fila = document.createElement("tr");
                    fila.innerHTML = `<td><a href="${member.url}">${member.first_name}</a></td><td>${member.party}</td><td>${member.state}</td><td>${member.seniority}</td><td>${member.votes_with_party_pct}</td>`;
                    tabla.appendChild(fila);
                    }
                }   
                );
        })

}
}));

//-------------------------------------------------------------------------
//Funcion para crear tabla pasandole por parametro un state
function renderState(x){
    dataHouse.results[0].members.forEach(member =>{
        if (member.state == x){
            let fila = document.createElement("tr");
        fila.innerHTML = `<td><a href="${member.url}">${member.first_name}</a></td><td>${member.party}</td><td>${member.state}</td><td>${member.seniority}</td><td>${member.votes_with_party_pct}</td>`;
        tabla.appendChild(fila);
        }
        
    });
}

//-------------------------------------------------------------------------
//Cuando el cliente interactua con lso checkbox empezamos a trabajar con ellos 
//y con los options si es que algun option esta selected

checkboxs.forEach(checkbox => checkbox.addEventListener('change',function(){
    tabla.innerHTML= "";
    let loschecket = (Array.from(checkboxs).filter(checkbox => checkbox.checked));
    let checkeados = loschecket.map(checkeado => checkeado.value)
    let seleccionado = [];
    
    opciones.forEach(opcion => {
        //Preguntamos si el value del option selected es diferente del value vacio 
        //y si la condicion es true, guardamos el option en un array vacio
        if(opcion.selected == true && opcion.value !== ""){
            seleccionado.push(opcion);
        }});
        //Si hay un elemento en el array y el array de cheackeados esta vacio renderizamos con es state
        if(seleccionado.length > 0 && checkeados.length == 0){
            renderState(seleccionado[0].value)
        }else if(seleccionado.length > 0){
        //Si hay un elemento en el array y el array de cheackeados no esta vacio renderizamos con es state y el checked
            checkeados.forEach(checkeado => renderDos(checkeado,seleccionado[0].value))
        }else if(checkeados.length > 0){
        //Si el array esta vacio, y el array de checkeados no esta vacio, renderizamos con el checked
            checkeados.forEach(checkeado => render(checkeado))
        }else if(checkeados.length == 0){
        //Si el array esta vacio, y el array de checkeados esta vacio, creamos la tabla con todos los miembros
            dataHouse.results[0].members.forEach(member =>{
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${member.url}">${member.first_name}</a></td><td>${member.party}</td><td>${member.state}</td><td>${member.seniority}</td><td>${member.votes_with_party_pct}</td>`;
                tabla.appendChild(fila);
            })
        }
}));
//--------------------------------------------------------------------
//Funcion para renderizar con el checked de parametro
function render(x){
    if(x === "R"){
        dataHouse.results[0].members.forEach(members =>{
            if(members.party == "R"){
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
                tabla.appendChild(fila);
            }  
    });
    }else if(x ==="D"){
        dataHouse.results[0].members.forEach(members =>{
            if(members.party == "D"){
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
                tabla.appendChild(fila);
            }
        });
    }else if(x === "I"){
        dataHouse.results[0].members.forEach(members =>{
            if(members.party == "I"){
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
                tabla.appendChild(fila);
            }     
    });
    }
}
//--------------------------------------------------------------------
//Funcion para renderizar con el checked y el state de parametro
function renderDos(x,state){
    if(x === "R"){
        dataHouse.results[0].members.forEach(members =>{
            if(members.party == "R" && members.state == state){
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
                tabla.appendChild(fila);
            }  
    });
    }else if(x ==="D"){
        dataHouse.results[0].members.forEach(members =>{
            if(members.party == "D" && members.state == state){
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
                tabla.appendChild(fila);
            }
        });
    }else if(x === "I"){
        dataHouse.results[0].members.forEach(members =>{
            if(members.party == "I" && members.state == state){
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${members.url}">${members.first_name}</a></td><td>${members.party}</td><td>${members.state}</td><td>${members.seniority}</td><td>${members.votes_with_party_pct}</td>`;
                tabla.appendChild(fila);
            }     
    });
    }
}


