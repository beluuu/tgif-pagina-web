/*
En particular, surgen muchas preguntas al comparar cómo votan los miembros de cada partido. Les gustaría algunos
estadísticas simples recopiladas y mostradas en una bonita tabla legible en páginas web para el Senado y la Cámara. Ellos
quisiera que juntaras tablas con:
el número de demócratas, republicanos e independientes
cómo los demócratas y los republicanos se comparan, en promedio, para votar con su partido
qué miembros con mayor frecuencia no votan con su partido, cuáles con mayor frecuencia votan con su partido
qué miembros han perdido más votos, cuáles han perdido menos
Por ejemplo, considere los votos perdidos. No quieren encontrar a la persona que más echó de menos. Ellos quieren una lista
del peor 10%, por lo que solo necesita ordenar los votos perdidos y tomar el 10% superior.*/
//CREO VARIABLES PARA UTILIZAR EN LA PRIMER TABLA
var totalMembers= 0;

var numberOfRepublicans=  0;
var numberOfDemocrats=  0;
var numberOfIndependents=  0;
var averagePercentPartyVotesRepublican=  0;
var averagePercentPartyVotesDemocrat=  0;
var averagePercentPartyVotesIndependent=  0;

var arrayOfRepublicans = [];
var arrayOfDemocrats = [];
var arrayOfIndependents = [];
var arrayOfRepubVotes = 0
var numRepubMember = 0;
var arrayOfDemocratVotes = 0
var numDemoMember = 0;
var arrayOfIndepVotes = 0;
var numIndepMember = 0;
//FUNCION ANONIMA QUE NOS SEPARAS LOS DATOS DE CADA PARTIDO
(() =>{
    dataHouse.results[0].members.forEach(members =>{ 
        
                if (members.party == "R") {
                    arrayOfRepublicans.push(members);
                    arrayOfRepubVotes += members.votes_with_party_pct;
                    numRepubMember += 1;
                } else if (members.party == "D") {
                    arrayOfDemocrats.push(members);
                    arrayOfDemocratVotes += members.votes_with_party_pct;
                    numDemoMember += 1;
                } else if (members.party == "ID") {
                    arrayOfIndependents.push(members);
                    arrayOfIndepVotes += members.votes_with_party_pct;
                    numIndepMember += 1;
                }
            });
})();
//REDEFINIMOS LAS VARIABLES CON LOS DATOS ANTERIORES
numberOfRepublicans = arrayOfRepublicans.length;
numberOfDemocrats = arrayOfDemocrats.length;
numberOfIndependents = arrayOfIndependents.length;
averagePercentPartyVotesDemocrat = (arrayOfDemocratVotes / numDemoMember).toFixed(2);
averagePercentPartyVotesRepublican = (arrayOfRepubVotes / numRepubMember).toFixed(2);
totalMembers = numRepubMember + numDemoMember + numIndepMember;
let totalMembersPercent = ((arrayOfDemocratVotes + arrayOfIndepVotes + arrayOfRepubVotes) / totalMembers).toFixed(2);
//-----Elementos HTML de la primer tabla CREAMOS LA TABLA
var republicanTotal = document.getElementById("republican-total");
republicanTotal.innerHTML = numberOfRepublicans;
var republicanPorc = document.getElementById("republican-porc");
republicanPorc.innerHTML = `${averagePercentPartyVotesRepublican}%`;

var democratTotal = document.getElementById("democrat-total");
democratTotal.innerHTML = numberOfDemocrats;
var democratPorc = document.getElementById("democrat-porc");
democratPorc.innerHTML = `${averagePercentPartyVotesDemocrat}%`;

var indepentTotal = document.getElementById("indepent-total");
indepentTotal.innerHTML = numberOfIndependents;
var indepentPorc = document.getElementById("indepent-porc");
indepentPorc.innerHTML = `${averagePercentPartyVotesIndependent}%`;

var total = document.getElementById("total-sum");
total.innerHTML = totalMembers;
var totalPorc = document.getElementById("total-porc")
totalPorc.innerHTML = `${totalMembersPercent}%`;


//------------ATTENDANCE
//------ELEMENTOS HTML LEAST ATTENDANCE
var attendanceLeastTable = document.getElementById("tbody-least");
//-------LEAST ATTENDANCE
var miembrosLeast = Array.from(dataHouse.results[0].members);
let leastAttendanceArray = [];
//FUNCION ANONIMA QUE ORDENA LOS MIEMBROS POR LOS VOTOS PERDIDOS DE MAYOR A MENOR
// Y SOLO VUELCA EN LA TABLA EL 10% DE LOS MIEBROS (45)
(() =>{
            miembrosLeast.sort(function (a, b) {
                return b.missed_votes_pct - a.missed_votes_pct
            })
            
            let el10porc = Math.ceil((miembrosLeast.length * 0.1))

            for (var i = 0; i < el10porc; i++) {

                leastAttendanceArray.push(miembrosLeast[i]);
            }
            let ultimoElem = leastAttendanceArray[leastAttendanceArray.length - 1];

            for (var l = el10porc; l < miembrosLeast.length; l++) {

                if (miembrosLeast[l].votes_with_party_pct === ultimoElem.votes_with_party_pct) {
                    leastAttendanceArray.push(miembrosLeast[l]);
                }
            }
            leastAttendanceArray.forEach(miembro =>{
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${miembro.url}">${miembro.first_name} ${miembro.last_name}</a></td><td>${miembro.missed_votes}</td><td>${miembro.missed_votes_pct}%</td>`
                attendanceLeastTable.appendChild(fila)
            })
        })();

//ELEMENTOS HTML MOST
var attendanceMostTable = document.getElementById("tbody-most")
//----------------MOST ATTENDANCE
var miembrosMost = Array.from(dataHouse.results[0].members);
let mostAttendanceArray = [];
(()=>{
    miembrosMost.sort(function (a, b) {
                return a.missed_votes_pct - b.missed_votes_pct
            })
            let el10porcMost = Math.ceil((miembrosMost.length * 0.1))

            for (var i = 0; i < el10porcMost; i++) {
                if(miembrosMost[i].missed_votes > 0){
                    mostAttendanceArray.push(miembrosMost[i]);
                }
            }
            let ultimoElem = mostAttendanceArray[mostAttendanceArray.length - 1];

            for (var j = el10porcMost; j < miembrosMost.length; j++) {

                if (miembrosMost[j].votes_with_party_pct === ultimoElem.votes_with_party_pct) {
                    mostAttendanceArray.push(miembrosMost[j]);
                }
            }
            
            mostAttendanceArray.forEach(miembro =>{
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${miembro.url}">${miembro.first_name} ${miembro.last_name}</a></td><td>${miembro.missed_votes}</td><td>${miembro.missed_votes_pct}%</td>`
                attendanceMostTable.appendChild(fila)
            })
})();
         