var totalMembers= 0;

var numberOfRepublicans=  0;
var numberOfDemocrats=  0;
var numberOfIndependents=  0;
var averagePercentPartyVotesRepublican=  0;
var averagePercentPartyVotesDemocrat=  0;
var averagePercentPartyVotesIndependent=  0;

var arrayOfRepublicans = [];
var arrayOfDemocrats = [];
var arrayOfIndependents = [];
var arrayOfRepubVotes = 0
var numRepubMember = 0;
var arrayOfDemocratVotes = 0
var numDemoMember = 0;
var arrayOfIndepVotes = 0;
var numIndepMember = 0;
//FUNCION ANONIMA QUE NOS SEPARAS LOS DATOS DE CADA PARTIDO
(() =>{
    dataHouse.results[0].members.forEach(members =>{ 
        
                if (members.party == "R") {
                    arrayOfRepublicans.push(members);
                    arrayOfRepubVotes += members.votes_with_party_pct;
                    numRepubMember += 1;
                } else if (members.party == "D") {
                    arrayOfDemocrats.push(members);
                    arrayOfDemocratVotes += members.votes_with_party_pct;
                    numDemoMember += 1;
                } else if (members.party == "ID") {
                    arrayOfIndependents.push(members);
                    arrayOfIndepVotes += members.votes_with_party_pct;
                    numIndepMember += 1;
                }
            });
})();
//REDEFINIMOS LAS VARIABLES CON LOS DATOS ANTERIORES
numberOfRepublicans = arrayOfRepublicans.length;
numberOfDemocrats = arrayOfDemocrats.length;
numberOfIndependents = arrayOfIndependents.length;
averagePercentPartyVotesDemocrat = (arrayOfDemocratVotes / numDemoMember).toFixed(2);
averagePercentPartyVotesRepublican = (arrayOfRepubVotes / numRepubMember).toFixed(2);
totalMembers = numRepubMember + numDemoMember + numIndepMember;
let totalMembersPercent = ((arrayOfDemocratVotes + arrayOfIndepVotes + arrayOfRepubVotes) / totalMembers).toFixed(2);
//-----Elementos HTML de la primer tabla CREAMOS LA TABLA
var republicanTotal = document.getElementById("republican-total");
republicanTotal.innerHTML = numberOfRepublicans;
var republicanPorc = document.getElementById("republican-porc");
republicanPorc.innerHTML = `${averagePercentPartyVotesRepublican}%`;

var democratTotal = document.getElementById("democrat-total");
democratTotal.innerHTML = numberOfDemocrats;
var democratPorc = document.getElementById("democrat-porc");
democratPorc.innerHTML = `${averagePercentPartyVotesDemocrat}%`;

var indepentTotal = document.getElementById("indepent-total");
indepentTotal.innerHTML = numberOfIndependents;
var indepentPorc = document.getElementById("indepent-porc");
indepentPorc.innerHTML = `${averagePercentPartyVotesIndependent}%`;

var total = document.getElementById("total-sum");
total.innerHTML = totalMembers;
var totalPorc = document.getElementById("total-porc")
totalPorc.innerHTML = `${totalMembersPercent}%`;


//-------------LOYAL LEAST
//-------------ELEMENTOS HTML
var tablaLeastLoyal = document.getElementById("tbody-least-loyal")
//-----ARRAYS DE MIEBROS LOYAL LEAST
var leastLoyalNames = [];
var miembrosLeastLoyal = Array.from(dataHouse.results[0].members);
//--------FUNCION ANONIMA QUE ORDENA DE MENOS A MAYOR LOS MIEBROS CON VOTOS CON PARTIDO
//-----Y VUELCA EN LA TABLA SOLO EL 10% (45)
(() =>{
    miembrosLeastLoyal.sort(function (a, b) {
                return a.votes_with_party_pct - b.votes_with_party_pct
            })
            var el10porc = Math.ceil((miembrosLeastLoyal.length * 0.1))

            for (var i = 0; i < el10porc; i++) {
                if(miembrosLeastLoyal[i].total_votes > 0){
                leastLoyalNames.push(miembrosLeastLoyal[i]);
                }
            }
            let ultimoElem = leastLoyalNames[leastLoyalNames.length - 1];

            for (var i = el10porc; i < miembrosLeastLoyal.length; i++) {

                if (miembrosLeastLoyal[i].votes_with_party_pct === ultimoElem.votes_with_party_pct) {
                    leastLoyalNames.push(miembrosLeastLoyal[i]);
                }
            }
            
            leastLoyalNames.forEach(miembro =>{
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${miembro.url}">${miembro.first_name} ${miembro.last_name}</a></td><td>${miembro.total_votes}</td><td>${miembro.votes_with_party_pct}%</td>`
                tablaLeastLoyal.appendChild(fila)
            })
})();
//------LOYAL MOST
//---------ELEMENTOS HTML
var tablaMostLoyal = document.getElementById("tbody-most-loyal");
//------ARRAYS DE MIEBROS
var mostLoyalNames = []
var miembrosMostLoyal = Array.from(dataHouse.results[0].members);
//FUNCION ANONIMA QUE ORDENA DE MAYOR A MENOR POR VOTOS POR PARTIDO DE MIEMBROS
//Y VUELVA LOS DATOS EN LA TABLA SOLO EL 10%(11MIEMBROS AL MENOS)
(() =>{
        miembrosMostLoyal.sort(function (a, b) {
                return b.votes_with_party_pct - a.votes_with_party_pct
            })
            var el10porc = Math.ceil((miembrosMostLoyal.length * 0.1))

            for (var i = 0; i < el10porc; i++) {

                mostLoyalNames.push(miembrosMostLoyal[i]);

            }
            var ultimoElem = mostLoyalNames[mostLoyalNames.length - 1];

            for (var j = el10porc; j < miembrosMostLoyal.length; j++) {

                if (miembrosMostLoyal[j].votes_with_party_pct === ultimoElem.votes_with_party_pct) {
                    mostLoyalNames.push(miembrosMostLoyal[j]);
                }
            }
            mostLoyalNames.forEach(miembro =>{
                let fila = document.createElement("tr");
                fila.innerHTML = `<td><a href="${miembro.url}">${miembro.first_name} ${miembro.last_name}</a></td><td>${miembro.total_votes}</td><td>${miembro.votes_with_party_pct}%</td>`
                tablaMostLoyal.appendChild(fila)
            })
})();
            